import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import LoginScreen from "./LoginScreen";
import AboutScreen from "./AboutScreen";
import {ProjectScreen} from "./ProjectScreen";
import {AddScreen} from "./AddScreen";

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Root = ({ navigation }) => {
  // const { signOut } = React.useContext(AuthContext);
  return (
    <ScreenContainer>
      <Text>Root</Text>
      <Button title="Drawer" onPress={() => navigation.toggleDrawer()} />
      <Button title="Login" onPress={() => navigation.push("LoginScreen")} />
    </ScreenContainer>
  );
};

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="ProjectScreen" component={ProjectScreen} />
    <Tabs.Screen name="AddScreen" component={AddScreen} />
  </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Profile">
    {/* <Drawer.Screen name="Home" component={TabsScreen} /> */}
    <Drawer.Screen name="Root" component={Root} />
    <Drawer.Screen name="Tabs" component={TabsScreen} />
    <Drawer.Screen name="About" component={AboutScreen} />
  </Drawer.Navigator>
);

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator>
    <RootStack.Screen
      name="Root"
      component={DrawerScreen}
      options={{
        animationEnabled: false,
      }}
    />
      <RootStack.Screen
      name="LoginScreen"
      component={LoginScreen}
      options={{ title: "Sign In AMD" }}
    />
  </RootStack.Navigator>
);

export default function App() {
  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
