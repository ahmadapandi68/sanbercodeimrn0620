import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  Button,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
export default class DaftarScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <Image
            source={require("./images/logo_putih.png")}
            style={{ width: 375, height: 102 }}
          />
        </View>
        <View style={styles.body}>
          <View style={styles.headerLogo}>
            <Text style={styles.textLogo}>Register</Text>
          </View>
          <Text style={styles.labelText}>Username</Text>
          <View style={styles.inputView}>
            <TextInput
              style={styles.inputText}
              placeholderTextColor="#003f5c"
              onChangeText={(text) => this.setState({ username: text })}
            />
          </View>
          <Text style={styles.labelText}>Email</Text>
          <View style={styles.inputView}>
            <TextInput
              style={styles.inputText}
              placeholderTextColor="#003f5c"
              onChangeText={(text) => this.setState({ email: text })}
            />
          </View>
          <Text style={styles.labelText}>Password</Text>
          <View style={styles.inputView}>
            <TextInput
              style={styles.inputText}
              placeholderTextColor="#003f5c"
              onChangeText={(text) => this.setState({ password: text })}
            />
          </View>
          <Text style={styles.labelText}>Ulangi Password</Text>
          <View style={styles.inputView}>
            <TextInput
              style={styles.inputText}
              placeholderTextColor="#003f5c"
              onChangeText={(text) => this.setState({ ulangiPassword: text })}
            />
          </View>
          <TouchableOpacity style={styles.daftarBtn}>
            <Text style={styles.loginText}> Daftar </Text>
          </TouchableOpacity>
          <Text style={styles.atauText}>Atau</Text>
          <TouchableOpacity style={styles.masukBtn}>
            <Text style={styles.loginText}> Masuk ?</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E5E5E5",
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    flex: 1,
  },
  inputBox: {
    // width: 300,
    backgroundColor: "rgba(255, 255,255,0.2)",
    // borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: "#ffffff",
    marginVertical: 10,
  },

  button: {
    // width: 300,
    backgroundColor: "#1c313a",
    // borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: "500",
    color: "#ffffff",
    textAlign: "center",
  },
  inputView: {
    width: "100%",
    backgroundColor: "#FFFFFF",
    // backgroundColor: "#465881",
    // borderRadius: 25,
    height: 50,
    marginBottom: 20,
    // marginLeft: 40,
    justifyContent: "center",
    padding: 20,
  },
  headerLogo: {
    width: "100%",
    alignItems: "center",
    height: 50,
    marginBottom: 20,
    justifyContent: "center",
    alignContent: "center",
    padding: 20,
    fontFamily: "Roboto",
    fontSize: 16,
    fontWeight: "500",
    marginTop: 64,
    // lineHeight: 28,
    // color: "#003366",
  },
  textLogo: {
    fontFamily: "Roboto",
    fontSize: 24,
    // fontWeight: "500"
  },
  inputText: {
    height: 50,
    color: "black",
  },
  labelText: {
    // marginLeft: 40,
  },
  loginText: {
    color: "white",
  },
  masukBtn: {
    width: "100%",
    alignSelf: "center",
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#3EC6FF",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    // marginTop: 40,
    marginBottom: 5,
  },
  atauText: {
    alignSelf: "center",
    fontFamily: "Roboto",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: 24,
    lineHeight: 28,
    color: "#3EC6FF",
  },
  daftarBtn: {
    width: "100%",
    alignSelf: "center",
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#465881",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    marginTop: 40,
    marginBottom: 10,
    marginTop: 5,
    color: "white",
  },
});
