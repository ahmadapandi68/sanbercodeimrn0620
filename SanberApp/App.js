import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button} from 'react-native';
import Component from './Latihan/Component/Component';
import YutubeUi from './Tugas/Tugas12/App';
import DaftarScreen from './Tugas/Tugas13/DaftarScreen';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import StateModul from './Tugas/Tugas14/App';
import Tugas15 from './Tugas/Tugas15/index';
import TugasNavigation from './Tugas/TugasNavigation/index';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Quiz3 from './Tugas/Quiz3/index';

// function HomeScreen({ navigation }) {
//   return (
//     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//       <Text>Home Screen</Text>
//       <Button
//         title="Go to About"
//         onPress={() => navigation.navigate('About')}
//       />
//       <Button
//         color="red"
//         title="Go to Skill"
//         onPress={() => navigation.navigate('Skill')}
//       />

//       <Button
//         color="red"
//         title="Go to Login"
//         onPress={() => navigation.navigate('Login')}
//       />

//       <Button
//         color="red"
//         title="Go to Daftar"
//         onPress={() => navigation.navigate('Daftar')}
//       />

//       <Button
//         color="red"
//         title="Go to Yutub"
//         onPress={() => navigation.navigate('Yutub',{'key':"value"})}
//       />
//     </View>
//   );
// }

// Stack berguna untuk routing aplikasi
// const Stack = createStackNavigator();

// function App() {
//   return (
//     <NavigationContainer>
//       <Stack.Navigator initialRouteName="Home">
//         <Stack.Screen name="Home" component={HomeScreen} />
//         <Stack.Screen name="About" component={StateModul} />
//         <Stack.Screen name="Skill" component={AboutScreen} />
//         <Stack.Screen name="Login" component={LoginScreen} />
//         <Stack.Screen name="Daftar" component={DaftarScreen} />
//         <Stack.Screen name="Yutub" component={YutubeUi} />
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// }
// export default App;

export default function App(){
// const app =()  => {
  return (
    <Quiz3 />
    // <TugasNavigation/>
    // <Tugas15/>
    // <StateModul>
    // </StateModul>
    // <AboutScreen />
    // <LoginScreen />
    // <DaftarScreen />
    // <YutubeUi/>
    // <Component />
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
