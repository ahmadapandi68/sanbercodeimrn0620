/* Contoh: jika input nya adalah [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]
maka outputnya di console seperti berikut :
1. Abduh Muhamad : { firstName: "Abduh", lastName: "Muhamad", gender: "male", age: 28}
2. Ahmad Taufik : { firstName: "Ahmad", lastName: "Taufik", gender: "male", age: 35} 
Untuk mendapatkan tahun sekarang secara otomatis bisa gunakan Class Date dari Javascript. */

// var now = new Date()
// var thisYear = now.getFullYear() // 2020 (tahun sekarang)
// Code

function arrayToObject(arr) {
    // Code di sini
    let i=0;
    let objOut;
    function getAge(birthYear){
        let now = new Date();
        let thisYear = now.getFullYear(); // 2020 (tahun sekarang)
        if(!birthYear || birthYear>thisYear){
            return "Invalid Birth Year";
        }
        return thisYear-birthYear;
    }
    while(arr[i]){
        let key;
        objOutf={};
        objOut={
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: getAge(arr[i][3])
        }
        key=`${(i+1)} ${objOut.firstName} ${objOut.lastName}`;
        console.log(key);
        objOutf[key]=objOut; 
        console.log( objOutf);
        i++;
    } 

} 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    if(money<50000){
        return "Mohon maaf, uang tidak cukup";
    }
    let listPurchased=[];
    let changeMoney=money;
    let outPutTrolli={"memberId":memberId,"money":money};
    let listProduk= [{"nama":"Sepatu brand Stacattu seharga" ,"harga": "1500000"},
                    {"nama":"Baju brand Zoro seharga" ,"harga": "500000"},
                    {"nama":"Baju brand H&N seharga" ,"harga": "250000"},
                    {"nama":"Sweater brand Uniklooh seharga" ,"harga": "175000"},
                    {"nama":"Casing Handphone seharga" ,"harga":"50000"}];
    listProduk.forEach(produk => {
        if(changeMoney>=produk.harga){
            listPurchased.push(produk.nama);            
            changeMoney-=produk.harga;
        }
        
    });
    outPutTrolli.listPurchased=listPurchased;
    outPutTrolli.changeMoney=changeMoney;

return outPutTrolli;
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    let outPuut=[];
    function hitungOngkos(a, b){
        return (rute.indexOf(b)-rute.indexOf(a))*2000;
    }
    arrPenumpang.forEach(penumpang => {
        let tmpOut={};
        tmpOut['penumpang']=penumpang[0];
        tmpOut['naikDari']=penumpang[1];
        tmpOut['tujuan']=penumpang[2];
        tmpOut['bayar']=hitungOngkos(penumpang[1],penumpang[2]);
        outPuut.push(tmpOut);
    });
    return outPuut;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]