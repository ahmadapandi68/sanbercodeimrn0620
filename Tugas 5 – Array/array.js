/* Soal No. 1 (Range) 
Buatlah sebuah function dengan nama range() yang menerima dua parameter berupa number.
Function mengembalikan sebuah array yang berisi angka-angka mulai dari angka parameter pertama hingga angka pada parameter kedua.
Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending).

struktur fungsinya seperti berikut range(startNum, finishNum) {}
Jika parameter pertama dan kedua tidak diisi maka function akan menghasilkan nilai -1
 */
// Code di sini
function range(starNum, finishNum) {
    if(!finishNum){
        return -1;
    }
    let array=[];
    let a = starNum;
    let b = finishNum;
    if(starNum>finishNum){
        b=a;
        a=finishNum;
    }
    
    while(a<=b){
        array.push(a);
        a++;
    }
    if(starNum>finishNum){
        array.sort(function (a, b) { return b - a } ) ;
    }
return array;
}
console.log("Range")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

/* Soal No. 2 (Range with Step)
Pada soal kali ini kamu diminta membuat function rangeWithStep yang mirip dengan function range di soal sebelumnya 
namun parameternya ditambah dengan parameter ketiga yaitu angka step yang menyatakan selisih atau beda dari setiap angka pada array.
Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending) 
dengan step sebesar parameter ketiga.

struktur fungsinya seperti berikut rangeWithStep(startNum, finishNum, step) {}
 */
// Code di sini
function rangeWithStep(starNum, finishNum, step) {
    if(!starNum || !finishNum){
        return -1;
    }
    let array=[];
    let a = starNum;
    let b = finishNum;
    if(starNum>finishNum){
        b=a;
        a=finishNum;
    }
    
    while(a<=b){
        array.push(a);
        a+=step;
    }
    if(starNum>finishNum){
        array.sort(function (a, b) { return b - a } ) ;
    }
return array;
}
console.log("RangewithStep")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal No. 3 (Sum of Range)
/* Kali ini kamu akan menjumlahkan sebuah range (Deret) yang diperoleh dari function range di soal-soal sebelumnya. 
Kamu boleh menggunakan function range dan rangeWithStep pada soal sebelumnya untuk menjalankan soal ini.
Buatlah sebuah function dengan nama sum() yang menerima tiga parameter yaitu angka awal deret,
angka akhir deret, dan beda jarak (step). Function akan mengembalikan nilai jumlah (sum) dari deret angka.
contohnya sum(1,10,1) akan menghasilkan nilai 55.

ATURAN: Jika parameter ke-3 tidak diisi maka stepnya adalah 1. */
function sum(a,b,c) {
    let val=0;
    if(!a){
        val++;
    }
    if(!b){
        val++;
    }
    if(!c){
        val++;
        c=1;
    }

    let array = rangeWithStep(a,b,c);
    let output = 0;
    let i=0;

    while(array[i]){
        output+=array[i];
        i++;
    }
    if(val>1){
        output= a;
    }
    output = output || 0
    return output;
}
// Code di sini
console.log("SUM")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

/* Soal No. 4 (Array Multidimensi)
Sering kali data yang diterima dari database adalah array yang multidimensi (array di dalam array).
Sebagai developer, tugas kita adalah mengolah data tersebut agar dapat menampilkan informasi yang diinginkan.
Buatlah sebuah fungsi dengan nama dataHandling dengan sebuah parameter untuk menerima argumen. 
Argumen yang akan diterima adalah sebuah array yang berisi beberapa array sejumlah n. Contoh input dapat dilihat dibawah: */
//contoh input
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 
// Tugas kamu adalah mengimplementasikan fungsi dataHandling() agar dapat menampilkan data-data pada dari argumen seperti di bawah ini:
// Nomor ID:  0001
// Nama Lengkap:  Roman Alamsyah
// TTL:  Bandar Lampung 21/05/1989
// Hobi:  Membaca
let i =0;
while(input[i]){
    console.log("Nomor ID: "+input[i]['0']);
    console.log("Nama Lengkap: "+input[i]['1']);
    console.log("TTL: "+input[i]['2']+" "+input[i]['3']);
    console.log("Hobi: "+input[i]['4']);
    i++;
}

/* Soal No. 5 (Balik Kata)
Kamu telah mempelajari beberapa method yang dimiliki oleh String dan Array.
String sebetulnya adalah sebuah array karena kita dapat mengakses karakter karakter pada sebuah string layaknya mengakses elemen pada array.
Buatlah sebuah function balikKata() yang menerima sebuah parameter berupa string dan mengembalikan kebalikan dari string tersebut. */
// Code di sini
function balikKata(kata) {
    let atak="";
    i=kata.length-1;
    while(kata[i]){
        atak+=kata[i];
        i--;
    }
    return atak;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
// Dilarang menggunakan sintaks .split , .join , .reverse() , hanya gunakan looping!

/* Soal No. 6 (Metode Array) */
let inputHandling2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 
function dataHandling2(params) {
    params[1] =params[1]+" Elsharawy"; 
    params[2] ="Provinsi "+ params[2]
    params.pop();
    params.push("Pria");
    params.push("SMA Internasional Metro")

    function getBulan(bulan) {
        var bulanOut;
    switch (bulan) {
        case "01":
            bulanOut = "Januari";
            break;
         case "02":
             bulanOut = "Februari";
             break;
         case "03":
             bulanOut = "Maret";
             break;
         case "04":
             bulanOut = "April";
             break;
         case "05":
             bulanOut = "Mei";
             break;
         case "06":
             bulanOut = "Juni";
             break;
         case "07":
             bulanOut = "Juli";
             break;
         case "08":
             bulanOut = "Agustus";
             break;
         case "09":
             bulanOut = "September";
             break;
         case "10":
             bulanOut = "Oktober";
             break;
         case "11":
             bulanOut = "November";
             break;
         case "12":
             bulanOut = "Desember";
             break;
        default:
            bulanOut = "Bulan Salah";
            break;
        }
        return bulanOut;
    }
    console.log(params);
    console.log(getBulan(params[3].split('/')[1]));
    console.log(params[3].split('/').sort(function (a, b) { return b - a } ));
    console.log(params[3].split('/').join("-"));
    console.log(params[1].substring(0,15));
    return "";
}
dataHandling2(inputHandling2);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 