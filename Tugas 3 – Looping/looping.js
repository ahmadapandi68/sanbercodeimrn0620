/* Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax while. 
Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk membuat suatu looping yang menghitung maju dan menghitung mundur. 
Jangan lupa tampilkan di console juga judul ‘LOOPING PERTAMA’ dan ‘LOOPING KEDUA’.” */

console.log("LOOPING PERTAMA");
var love1 = 2;
while(love1 <= 20) { 
  console.log(love1+' - I love coding'); 
  love1+=2;
}

console.log("LOOPING KEDUA");
var love2 = 20;
while(love2 >= 2) { 
    console.log(love2+' - I will become a mobile developer'); 
    love2-=2;
  }

  console.log("\n");
/* No. 2 Looping menggunakan for
  Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax for.
  Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk memenuhi syarat tertentu yaitu:
  SYARAT:
  A. Jika angka ganjil maka tampilkan Santai
  B. Jika angka genap maka tampilkan Berkualitas
  C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding. */
  var kondisi="";
  for(var angka = 1; angka <= 20; angka++) {
    if((angka%2)==1){
        kondisi="Santai"
    }else{
        kondisi="Berkualitas"
    }
    if((angka%3==0) && (angka%2==1) ){
        kondisi="I Love Coding";
    }
    console.log(angka+" - "+kondisi);
  } 

  console.log("\n");
/*Kamu diminta untuk menampilkan persegi dengan dimensi 8×4 dengan tanda pagar (#) dengan perulangan atau looping.
 Looping boleh menggunakan syntax apa pun (while, for, do while). */
 var looppesrsegi=1;
 while(looppesrsegi<=4){
    console.log("########"); 
    looppesrsegi++;
 }

 console.log("\n");
 /*No. 4 Membuat Tangga 
Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi 7 dan alas 7. 
Looping boleh menggunakan syntax apa pun (while, for, do while).*/
  var looptangga=1;
  var anaktangga=1;
  var bata="";
  while(looptangga<=7){
    anaktangga=1;
    while(anaktangga<=looptangga){
        bata=bata+"#";
        anaktangga++;
    }
    console.log(bata);
    bata="";
     looptangga++;
  }

  console.log("\n");
/*No. 5 Membuat Papan Catur
  Buatlah suatu looping untuk menghasilkan sebuah papan catur dengan ukuran 8 x 8 . 
  Papan berwarna hitam memakai tanda pagar (#) sedangkan papan putih menggunakan spasi. 
  Looping boleh menggunakan syntax apa pun (while, for, do while). */
  var loopCatur=1;
  var anakCatur=1;
  var bataCatur="";
  while(loopCatur<=8){
    anakCatur=1;
    while(anakCatur<=8){
        if(anakCatur%2==loopCatur%2){
          bataCatur=bataCatur+" ";
        }else{
          bataCatur=bataCatur+"#";
        }
        anakCatur++;
    }
    console.log(bataCatur);
    bataCatur="";
     loopCatur++;
  }
